import React, { useMemo, useState } from 'react';
import styled from 'styled-components';
import { breakpoints } from '../../styles/theme';
import Button, { ButtonStyle } from '../components/Button';
import { HttpError, useFetch } from '../data/fetch.hook';
import {
  createCompleted,
  createErrored,
  createLoading,
  createUninitialised,
  Item,
} from '../data/item';
import { useInfraConfig } from '../infra-config/infra-config.hook';
import { CodeAreaItem } from './CodeItem';

const TestTriggerDisplay = styled.div`
  flex: 0 0 auto;
  padding: var(--spacing-small) 0;
  @media (min-width: ${breakpoints.small}) {
    padding: 0 var(--spacing-small);
  }
`;

interface TriggerProps {
  disabled: boolean;
  onTest: () => void;
}
const TestTrigger = ({ disabled, onTest }: TriggerProps) => {
  return (
    <Button style={ButtonStyle.Light} disabled={disabled} onClick={onTest}>
      Test
    </Button>
  );
};

const TestPayload = styled.div`
  flex: 1 0 auto;
  height: 210px;

  padding: var(--spacing-small) 0;
  width: 100%;
  @media (min-width: ${breakpoints.small}) {
    width: unset;
    padding: 0 var(--spacing-small);
  }
`;

const TestImageDisplay = styled.div`
  flex: 0 0 auto;
  padding: var(--spacing-small) 0;
  @media (min-width: ${breakpoints.small}) {
    padding: 0 var(--spacing-small);
  }
`;

const TestImg = styled.img`
  width: 200px;
  height: 200px;
`;

const TestWrapper = styled.div`
  padding: var(--spacing-base);
  border-radius: var(--borders-radius);
  background-color: var(--colors-contentBackground-dark);
`;

const PlaceholderImg = styled.div`
  outline: solid 1px var(--colors-contentBackground-light);
  width: 200px;
  height: 200px;
`;

interface ImageProps {
  photoUrl?: string;
}
const TestImage = ({ photoUrl }: ImageProps) => {
  if (!photoUrl) {
    return <PlaceholderImg></PlaceholderImg>;
  }

  return <TestImg src={photoUrl}></TestImg>;
};

const ItemWrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;

  flex-direction: column;
  @media (min-width: ${breakpoints.small}) {
    flex-direction: row;
  }
`;

const CodeArea = styled(CodeAreaItem)`
  height: 150px;
`;

const Endpoint = styled.div`
  max-width: 100%;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;

  color: var(--colors-text-small);
  font-size: var(--typography-size-small);
  font-family: monospace;
  text-align: end;
  padding-top: var(--spacing-small);
`;

interface TestPayload {
  id: string;
  name: string;
  photoUri: string;
}

const Tester = () => {
  const { fetch } = useFetch();
  const { serviceConfig } = useInfraConfig();
  const [result, setResult] = useState<Item<TestPayload>>(
    createUninitialised()
  );
  const payloadDisplay = useMemo(() => {
    if (result.error) {
      const err = result.error as HttpError;
      return JSON.stringify(
        {
          message: err.message,
          status: err.status,
        },
        null,
        2
      );
    }
    return result.value ? JSON.stringify(result.value, null, 2) : '';
  }, [result]);

  const serviceUrl = serviceConfig.value?.serviceUrl;
  const disabled = !serviceUrl || result.isLoading;

  const testEndpoint = () => {
    setResult(createLoading());
    fetch(`/api/cat/random`)
      .then((payload: TestPayload) => setResult(createCompleted(payload)))
      .catch((err) => setResult(createErrored(err)));
  };

  return (
    <span>
      <TestWrapper>
        <ItemWrapper>
          <TestTriggerDisplay>
            <TestTrigger
              disabled={disabled}
              onTest={testEndpoint}
            ></TestTrigger>
          </TestTriggerDisplay>
          <TestPayload>
            <CodeArea
              value={payloadDisplay}
              isLoading={result.isLoading}
              loadingLines={3}
            ></CodeArea>
          </TestPayload>
          <TestImageDisplay>
            <TestImage photoUrl={result.value?.photoUri}></TestImage>
          </TestImageDisplay>
        </ItemWrapper>
        {serviceUrl ? <Endpoint>{serviceUrl}</Endpoint> : null}
      </TestWrapper>
    </span>
  );
};

export default Tester;

import styled from 'styled-components';

interface Props {
  maxWidth: number;
  title: string;
  image: string;
}

const ImgItem = styled.img`
  width: 100%;
  height: auto;
`;

const ImgWrapper = styled.div`
  padding: var(--spacing-base) 0;
`;

const Img = ({ maxWidth, title, image }: Props) => {
  return (
    <ImgWrapper>
      <ImgItem
        src={`/assets/${image}.png`}
        title={title}
        alt={title}
        style={{ maxWidth: `${maxWidth}px` }}
      ></ImgItem>
    </ImgWrapper>
  );
};

export default Img;

import React, { useMemo } from 'react';
import styled, { css } from 'styled-components';
import { codeStyles } from '../../styles/theme';
import Placeholders, {
  SinglePlaceholder,
  PlaceholderDimensions,
} from '../components/Placeholder';

const areaCommon = css`
  display: block;
  width: 100%;
  height: 100%;
  max-height: 250px;
  background-color: var(--colors-contentBackground);
  padding: var(--spacing-tiny);
  outline: none;
  border-radius: var(--borders-radius);
`;

const CodeArea = styled.textarea`
  resize: none;
  color: var(--colors-text);
  border: none;
  ${areaCommon}
`;

interface AreaProps {
  value: string;
  isLoading?: boolean;
  loadingLines?: number;
}

const AreaPlaceholderWrapper = styled.span`
  ${areaCommon}
  overflow-y: hidden;
`;

const widths = [35, 42, 69, 58, 44, 73, 65, 54, 88, 65];

const getAreaPlaceholders = (count: number): PlaceholderDimensions[] => [
  { widthPercentage: 5, heightRems: 1.125 },
  ...[...Array.from(Array(count).keys())].map((idx) => ({
    widthPercentage: widths[idx % widths.length],
    heightRems: 1.125,
  })),
  { widthPercentage: 5, heightRems: 1.125 },
];

interface AreaPlaceholderProps {
  count: number;
}

const AreaItemPlaceholder = ({ count }: AreaPlaceholderProps) => {
  const areaPlaceholders = useMemo(() => getAreaPlaceholders(count), [count]);
  return (
    <AreaPlaceholderWrapper>
      <Placeholders placeholders={areaPlaceholders}></Placeholders>
    </AreaPlaceholderWrapper>
  );
};

export const CodeAreaItem = ({ value, isLoading, loadingLines }: AreaProps) => {
  if (isLoading) {
    return (
      <AreaItemPlaceholder count={loadingLines ?? 9}></AreaItemPlaceholder>
    );
  }

  return <CodeArea wrap="off" readOnly value={value}></CodeArea>;
};

const CodeItemWrapper = styled.code`
  ${codeStyles}
`;

interface CodeProps {
  isLoading?: boolean;
  value: string;
}

const ItemPlaceholder = styled(SinglePlaceholder)`
  display: inline-block;
  height: 1.125rem;
  width: 8rem;
  margin-bottom: -3px;
  border-radius: var(--borders-radius);
`;

export const CodeItem = ({ isLoading, value }: CodeProps) => {
  if (isLoading) {
    return <ItemPlaceholder></ItemPlaceholder>;
  }

  return <CodeItemWrapper>{value}</CodeItemWrapper>;
};

import styled from 'styled-components';
import { useInfraConfig } from '../infra-config/infra-config.hook';

interface ServiceContext {
  region: string;
  name: string;
}

interface GcpContext {
  service: ServiceContext;
}

interface Props {
  link: (ctx: GcpContext) => string;
  children: React.ReactNode | React.ReactNode[];
}

const GcpLink = ({ link, children }: Props) => {
  const { serviceConfig } = useInfraConfig();
  const service = serviceConfig.value;
  if (!service) {
    return <span>{children}</span>;
  }
  const projectId = service.projectId;

  const ctx: GcpContext = {
    service: {
      name: service.serviceName,
      region: service.region,
    },
  };
  const linkHref = `https://console.cloud.google.com/${link(
    ctx
  )}?project=${projectId}`;
  return (
    <a target="_blank" href={linkHref}>
      {children}
    </a>
  );
};

export default GcpLink;

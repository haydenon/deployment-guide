import React, { ReactNode, useMemo } from 'react';
import styled from 'styled-components';
import Tabs from '../components/Tabs';
import { FileChangeType } from '../development-walkthroughs/development-walkthrough';
import { Changes as CiChanges } from '../development-walkthroughs/gitlab-ci-walkthrough';
import { Changes as DockerChanges } from '../development-walkthroughs/dockerfile-walkthrough';

import {
  File,
  useWalkthrough,
} from '../development-walkthroughs/walkthrough.hook';
import {
  Preview,
  Range,
} from '../development-walkthroughs/walkthrough.service';
import { CodeAreaItem } from './CodeItem';

interface FileStep {
  file: File;
  step: number;
}

interface Props {
  step: FileStep;
}

interface LineProps {
  content: string;
  lineType?: FileChangeType;
}

const BaseLine = styled.span`
  padding: 2px var(--spacing-tiny);
  width: fit-content;
  line-height: 1.4;
`;
const AddLine = styled(BaseLine)`
  background-color: var(--colors-contentBackground-positive);
`;
const ModifyLine = styled(BaseLine)`
  background-color: var(--colors-contentBackground-neutral);
`;

const ChangeTypeContent = styled.span`
  user-select: none;
  padding: 0;
  margin: 0;
  display: inline;
  background-color: inherit;
`;

const ChangeType = ({ lineType }: { lineType?: FileChangeType }) => {
  switch (lineType) {
    case FileChangeType.Insertion:
      return (
        <AddLine>
          <ChangeTypeContent>{'+\n'}</ChangeTypeContent>
        </AddLine>
      );
    case FileChangeType.Modification:
      return (
        <ModifyLine>
          <ChangeTypeContent>{'*\n'}</ChangeTypeContent>
        </ModifyLine>
      );
    default:
      return (
        <BaseLine>
          <ChangeTypeContent>{' \n'}</ChangeTypeContent>
        </BaseLine>
      );
  }
};

const Line = ({ content, lineType }: LineProps) => {
  content = content + '\n';
  switch (lineType) {
    case FileChangeType.Insertion:
      return <AddLine>{content}</AddLine>;
    case FileChangeType.Modification:
      return <ModifyLine>{content}</ModifyLine>;
    default:
      return <BaseLine>{content}</BaseLine>;
  }
};

const PreviewWrapper = styled.div`
  display: flex;
  flex-direction: row;
  max-width: 100%;
`;

const PreviewType = styled.pre`
  margin: 0;
  user-select: none;
  padding-right: 0;
`;

const Contents = styled.pre`
  margin: 0;
  user-select: auto;
  padding-left: 0;
`;

const getChangePreview = (preview: Preview | undefined) => () => {
  if (!preview) {
    return <CodeAreaItem value={''} isLoading={true}></CodeAreaItem>;
  }
  const { previewContents, lineChangeTypes, displayChangeRanges } = preview;
  const getRangeContent = ([start, end]: Range) =>
    previewContents.slice(start, end + 1);
  const getTypeForLine = (line: number): FileChangeType | undefined =>
    lineChangeTypes[line];
  return (
    <PreviewWrapper>
      <PreviewType>
        {displayChangeRanges.map((range, rangeIdx) => (
          <React.Fragment key={rangeIdx}>
            {getRangeContent(range).map((_, idx) => (
              <ChangeType
                key={`${rangeIdx}-${idx}`}
                lineType={getTypeForLine(range[0] + idx)}
              ></ChangeType>
            ))}
          </React.Fragment>
        ))}
      </PreviewType>
      <Contents>
        {displayChangeRanges.map((range, rangeIdx) => (
          <React.Fragment key={rangeIdx}>
            {getRangeContent(range).map((line, idx) => (
              <Line
                key={`${rangeIdx}-${idx}`}
                lineType={getTypeForLine(range[0] + idx)}
                content={line}
              ></Line>
            ))}
          </React.Fragment>
        ))}
      </Contents>
    </PreviewWrapper>
  );
};

const getFullContents = (contents: string[] | undefined) => () => {
  if (!contents) {
    return <CodeAreaItem value={''} isLoading={true}></CodeAreaItem>;
  }
  return (
    <Contents>
      {contents.map((content, idx) => (
        <Line key={idx} content={content}></Line>
      ))}
    </Contents>
  );
};

interface WrapperProps {
  filename: string;
  content: () => React.ReactNode;
}

const Filename = styled.div`
  padding-top: var(--spacing-tiny);
  color: var(--colors-text-small);
  font-size: var(--typography-size-small);
  font-family: monospace;
  text-align: end;
`;

const ContentsWrapper = styled.div`
  overflow-x: auto;
  padding-bottom: var(--spacing-base);
`;

const TabWrapper = ({ filename, content }: WrapperProps) => {
  return (
    <div>
      <ContentsWrapper>{content()}</ContentsWrapper>
      <Filename>{filename}</Filename>
    </div>
  );
};

const Wrapper = styled.div`
  padding-bottom: var(--spacing-extraLarge);
`;

const FileChange = ({ step: { file, step } }: Props) => {
  const [filename, preview, contents] = useWalkthrough(file, step);
  const getWrapper = (content: () => ReactNode) => () =>
    <TabWrapper content={content} filename={filename}></TabWrapper>;
  const tabs = useMemo(
    () => [
      { name: 'Changes', panel: getWrapper(getChangePreview(preview)) },
      { name: 'Full content', panel: getWrapper(getFullContents(contents)) },
    ],
    [preview, contents]
  );
  return (
    <span>
      <Wrapper>
        <Tabs tabs={tabs}></Tabs>
      </Wrapper>
    </span>
  );
};

export const Ci = {
  AddStages: { file: File.Ci, step: CiChanges.AddStages },
  AddDeployJob: { file: File.Ci, step: CiChanges.AddDeployJob },
  AddExampleDeploy: { file: File.Ci, step: CiChanges.AddExampleDeploy },
  AddDeployJobImage: { file: File.Ci, step: CiChanges.AddDeployJobImage },
  AddDeployVariables: { file: File.Ci, step: CiChanges.AddDeployVariables },
  AddBuildStage: { file: File.Ci, step: CiChanges.AddBuildStage },
  AddBuildJob: { file: File.Ci, step: CiChanges.AddBuildJob },
  AddDockerBuild: { file: File.Ci, step: CiChanges.AddDockerBuild },
  AddDockerPush: { file: File.Ci, step: CiChanges.AddDockerPush },
  AddRegistryLogin: { file: File.Ci, step: CiChanges.AddRegistryLogin },
  ChangeToBuiltImage: {
    file: File.Ci,
    step: CiChanges.ChangeToBuiltImage,
  },
};

export const Docker = {
  AddFromSdk: { file: File.Docker, step: DockerChanges.AddFromSdk },
  AddWorkdDir: { file: File.Docker, step: DockerChanges.AddWorkdDir },
  CopySrc: { file: File.Docker, step: DockerChanges.CopySrc },
  Publish: { file: File.Docker, step: DockerChanges.Publish },
  SwitchWorkDir: { file: File.Docker, step: DockerChanges.SwitchWorkDir },
  AddEntryPoints: { file: File.Docker, step: DockerChanges.AddEntryPoints },
};

export default FileChange;

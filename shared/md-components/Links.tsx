import GcpLink from './GcpLink';

export const CloudRunLink = () => (
  <GcpLink
    link={(ctx) =>
      `run/detail/${ctx.service.region}/${ctx.service.name}/revisions`
    }
  >
    the Cloud Run service in the Google Cloud Console
  </GcpLink>
);

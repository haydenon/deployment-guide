import { ServiceConfig } from '../infra-config/infra-config';
import { useInfraConfig } from '../infra-config/infra-config.hook';
import React from 'react';
import { CodeAreaItem, CodeItem } from './CodeItem';
import styled from 'styled-components';

interface Props {
  dataSelector: (config: ServiceConfig) => any | undefined;
}

const ConfigDisplay = ({ dataSelector }: Props) => {
  const { serviceConfig } = useInfraConfig();
  const value = serviceConfig?.value;

  return (
    <CodeItem
      isLoading={!value}
      value={value ? dataSelector(value) : '...'}
    ></CodeItem>
  );
};

const serviceConfigItem =
  (selector: (conf: ServiceConfig) => any | undefined) => () =>
    <ConfigDisplay dataSelector={selector}></ConfigDisplay>;

const KeyCodeArea = styled.div`
  height: 250px;
  padding-bottom: var(--spacing-base);
`;

export const DeployKey = () => {
  const { deployKey } = useInfraConfig();
  const value = deployKey.value;
  return (
    <KeyCodeArea>
      <CodeAreaItem
        isLoading={!value}
        value={JSON.stringify(value, null, 2)}
      ></CodeAreaItem>
    </KeyCodeArea>
  );
};

export const ProjectId = serviceConfigItem((c) => c.projectId);
export const ServiceName = serviceConfigItem((c) => c.serviceName);
export const Region = serviceConfigItem((c) => c.region);
export const Registry = serviceConfigItem(
  (c) => c.repositoryBase.split('/')[0]
);
export const ExampleDeploy = serviceConfigItem(
  (c) =>
    `gcloud run deploy ${c.serviceName} --image=${c.repositoryBase}/example/app:automated`
);

interface RepositoryProps {
  repository?: string;
  image?: string;
}

export const Repository = ({ repository, image }: RepositoryProps) => {
  const imagePath = image ? `/${image}` : '';
  const registry = (serviceConfig: ServiceConfig) =>
    `${serviceConfig.repositoryBase}/${
      repository ?? serviceConfig.repositoryName
    }${imagePath}`;
  return <ConfigDisplay dataSelector={registry}></ConfigDisplay>;
};

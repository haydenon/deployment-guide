import React from 'react';
import styled from 'styled-components';

import { useUser } from '../user.hook';
import { UserLoginState } from '../user';
import SignIn from './SignIn';
import SignedIn from './SignedIn';

const Wrapper = styled.div`
  height: 58px;
  padding-bottom: 8px;
`;

const AccountNavState = () => {
  const { user } = useUser();
  const value = user.value;

  if (!value) {
    return null;
  }

  if (value.loginState != UserLoginState.LoggedIn) {
    return <SignIn></SignIn>;
  }

  return <SignedIn user={value.details}></SignedIn>;
};

const AccountNav = () => {
  return (
    <Wrapper>
      <AccountNavState></AccountNavState>
    </Wrapper>
  );
};

export default AccountNav;

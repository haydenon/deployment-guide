import React from 'react';
import GoogleLogin from 'react-google-login';

import { UserLoginState } from '../user';
import { useUser } from '../user.hook';

const SignIn = () => {
  const { user, handleSignin } = useUser();
  const value = user.value;

  if (!value) {
    return null;
  }

  return (
    <GoogleLogin
      clientId={process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID!}
      onSuccess={handleSignin}
      isSignedIn={value.loginState == UserLoginState.AwaitingDetails}
      buttonText="Sign in"
    />
  );
};

export default SignIn;

import React from 'react';
import { GoogleLogout } from 'react-google-login';
import styled from 'styled-components';
import Dropdown, {
  DropdownButton,
  DropdownDisplayItem,
} from '../../components/Dropdown';

import { UserDetails } from '../user';
import { useUser } from '../user.hook';

const LogoutWrapper = styled.div`
  padding: var(--spacing-small);
`;

const SignOut = () => {
  const { handleSignOut } = useUser();
  return (
    <LogoutWrapper>
      <GoogleLogout
        clientId={process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID!}
        onLogoutSuccess={handleSignOut}
        buttonText="Sign out"
      ></GoogleLogout>
    </LogoutWrapper>
  );
};

interface SignedInProps {
  user: UserDetails;
}

const ProfileImage = styled.img`
  border-radius: 50%;
  border: var(--borders-width) solid var(--colors-text);
  height: 50px;
  width: 50px;
`;

const position = (targetRect?: any, popoverRect?: any): React.CSSProperties => {
  if (!targetRect || !popoverRect) {
    return {};
  }

  return {
    top: `${targetRect.top + targetRect.height + window.pageYOffset + 10}px`,
    left: `${targetRect.right - popoverRect.width + window.pageXOffset}px`,
  };
};

const Button = styled(DropdownButton)`
  height: 50px;
`;

const SignedIn = ({ user }: SignedInProps) => {
  const renderButton = () => {
    return (
      <Button>
        <ProfileImage
          src={user.imageUrl}
          alt="User profile image"
        ></ProfileImage>
      </Button>
    );
  };
  return (
    <Dropdown renderButton={renderButton} position={position}>
      <DropdownDisplayItem>{user.givenName}</DropdownDisplayItem>
      <SignOut></SignOut>
    </Dropdown>
  );
};

export default SignedIn;

export enum UserLoginState {
  SignedOut,
  AwaitingDetails,
  LoggedIn,
}

export interface UserDetails {
  givenName: string;
  imageUrl: string;
}

interface LoginUserState {
  loginState: UserLoginState;
  tokenId?: string;
  details?: UserDetails;
}

interface LoggedInUser extends LoginUserState {
  loginState: UserLoginState.LoggedIn;
  tokenId: string;
  details: UserDetails;
}

interface AwaitingUser extends LoginUserState {
  loginState: UserLoginState.AwaitingDetails;
  tokenId: undefined;
  details: undefined;
}

interface LoggedOutUser extends LoginUserState {
  loginState: UserLoginState.SignedOut;
  tokenId: undefined;
  details: undefined;
}

export type UserState = LoggedInUser | AwaitingUser | LoggedOutUser;

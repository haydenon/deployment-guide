import { useCallback, useEffect } from 'react';
import {
  GoogleLoginResponse,
  GoogleLoginResponseOffline,
} from 'react-google-login';
import { useRecoilState, useRecoilValue } from 'recoil';

import { useDb } from '../data/db';
import { DbKey } from '../data/db-keys';
import { createCompleted, createLoading, ItemState } from '../data/item';
import { UserLoginState } from './user';
import { isSignedIn, unauthorisedState, userState } from './user.state';

const isOnlineResponse = (response: any): response is GoogleLoginResponse =>
  !!response.tokenId;

export const useUser = () => {
  const { getItem, setItem } = useDb();
  const [user, setUser] = useRecoilState(userState);

  useEffect(() => {
    if (user.state == ItemState.Uninitialised) {
      setUser(createLoading());
      getItem(DbKey.UserSignedIn).then((userSignedIn) => {
        setUser(
          createCompleted({
            loginState: !userSignedIn
              ? UserLoginState.SignedOut
              : UserLoginState.AwaitingDetails,
            tokenId: undefined,
            details: undefined,
          })
        );
      });
    }
  }, [user, getItem]);

  const handleSignin = useCallback(
    (response: GoogleLoginResponse | GoogleLoginResponseOffline) => {
      if (isOnlineResponse(response)) {
        setUser(
          createCompleted({
            loginState: UserLoginState.LoggedIn,
            tokenId: response.tokenId,
            details: {
              givenName: response.profileObj.givenName,
              imageUrl: response.profileObj.imageUrl,
            },
          })
        );
        setItem(DbKey.UserSignedIn, true);
      }
    },
    []
  );

  const handleSignOut = useCallback(() => {
    setUser(
      createCompleted({
        loginState: UserLoginState.SignedOut,
        tokenId: undefined,
        details: undefined,
      })
    );
    setItem(DbKey.UserSignedIn, false);
  }, [setUser, setItem]);
  const [isUnauthorised] = useRecoilState(unauthorisedState);
  const isAuthorised = !!user?.value?.tokenId;
  const signedIn = useRecoilValue(isSignedIn);

  return {
    user,
    isSignedIn: signedIn,
    isUnauthorised,
    isAuthorised,
    handleSignin,
    handleSignOut,
  };
};

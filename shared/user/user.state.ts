import { atom, selector } from 'recoil';
import {
  createUninitialised,
  isUninitialisedOrLoading,
  Item,
} from '../data/item';
import { getKeyGenerator, StateNamespace } from '../data/state-namespaces';
import { UserLoginState, UserState } from './user';

export enum StateKey {
  UserState = 'UserState',
  IsSignedIn = 'IsSignedIn',
  UserToken = 'UserToken',
  Unauthorised = 'Unauthorised',
}

const getKey = getKeyGenerator(StateNamespace.User);

export const userState = atom<Item<UserState>>({
  key: getKey(StateKey.UserState),
  default: createUninitialised(),
});

export const isSignedIn = selector({
  key: getKey(StateKey.IsSignedIn),
  get: ({ get }) => {
    const user = get(userState);

    return isUninitialisedOrLoading(user)
      ? undefined
      : user.value?.loginState !== UserLoginState.SignedOut;
  },
});

export const userToken = selector({
  key: getKey(StateKey.UserToken),
  get: ({ get }) => {
    const user = get(userState);

    return user.value?.tokenId;
  },
});

export const unauthorisedState = atom<boolean>({
  key: getKey(StateKey.Unauthorised),
  default: false,
});

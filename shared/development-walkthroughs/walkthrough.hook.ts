import { useMemo } from 'react';
import { ServiceConfig } from '../infra-config/infra-config';
import { useInfraConfig } from '../infra-config/infra-config.hook';
import { DevelopmentWalkthrough } from './development-walkthrough';
import { getDockerfileWalkthrough } from './dockerfile-walkthrough';
import { getGitlabCiWalkthrough } from './gitlab-ci-walkthrough';
import { Preview, walkthroughStepsService } from './walkthrough.service';

export enum File {
  Ci,
  Docker,
}

const getWalkthrough = (
  file: File,
  serviceConfig?: ServiceConfig
): DevelopmentWalkthrough => {
  switch (file) {
    case File.Ci:
      return getGitlabCiWalkthrough(serviceConfig);
    case File.Docker:
      return getDockerfileWalkthrough(serviceConfig);
    default:
      throw new Error(`Invalid walkthrough file ${File[file]}`);
  }
};

export const useWalkthrough = <Key extends number>(
  file: File,
  step: Key
): [string, Preview | undefined, string[] | undefined] => {
  const { serviceConfig } = useInfraConfig();
  const config = serviceConfig.value;
  const walkthrough = useMemo(
    () => getWalkthrough(file, config),
    [file, config]
  );

  return useMemo(() => {
    if (walkthrough.fileChangeSteps.length < 1) {
      return [walkthrough.filename, undefined, undefined];
    }
    const steps = walkthrough.fileChangeSteps.slice(0, step + 1);
    const service = walkthroughStepsService(steps);
    return [
      walkthrough.filename,
      service.getPreviewForStepChanges(),
      service.getFileContents(),
    ];
  }, [walkthrough, step]);
};

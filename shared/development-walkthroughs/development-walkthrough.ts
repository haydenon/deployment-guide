export enum FileChangeType {
  Insertion,
  Modification,
}

interface FileChangeBase {
  type: FileChangeType;
  lineNumber: number;
}

interface InsertChange extends FileChangeBase {
  type: FileChangeType.Insertion;
  contents: string[];
}

interface ModificationChange extends FileChangeBase {
  type: FileChangeType.Modification;
  replaceLines: number;
  contents: string[];
}

export const createInsert = (
  lineNumber: number,
  contents: string[]
): InsertChange => ({
  type: FileChangeType.Insertion,
  lineNumber,
  contents,
});

export const createModify = (
  lineNumber: number,
  contents: string[],
  replaceLines = 1
): ModificationChange => ({
  type: FileChangeType.Modification,
  lineNumber,
  contents,
  replaceLines,
});

export type FileChange = InsertChange | ModificationChange;

export interface DevelopmentWalkthrough {
  filename: string;
  fileChangeSteps: FileChange[][];
}

import { FileChange, FileChangeType } from './development-walkthrough';

const applyStep = (contents: string[], change: FileChange): string[] => {
  const { type, lineNumber } = change;
  switch (change.type) {
    case FileChangeType.Insertion: {
      const newContents = change.contents;
      return [
        ...contents.slice(0, lineNumber),
        ...newContents,
        ...contents.slice(lineNumber),
      ];
    }
    case FileChangeType.Modification: {
      const { contents: newContents, replaceLines } = change;
      return [
        ...contents.slice(0, lineNumber),
        ...newContents,
        ...contents.slice(lineNumber + replaceLines),
      ];
    }
    default:
      throw new Error(`Didn't handle change type ${FileChangeType[type]}`);
  }
};

const getFileContents = (
  steps: FileChange[][],
  contents: string[]
): string[] => {
  if (steps.length < 1) {
    return contents;
  }

  const nextSteps = steps[0];
  const nextContent = nextSteps.reduce(applyStep, contents);
  return getFileContents(steps.slice(1), nextContent);
};

const applyPreviewStep = (contents: string[], change: FileChange): string[] => {
  const { type, lineNumber } = change;
  switch (change.type) {
    case FileChangeType.Insertion: {
      const newContents = change.contents;
      return [
        ...contents.slice(0, lineNumber),
        ...newContents,
        ...contents.slice(lineNumber),
      ];
    }
    case FileChangeType.Modification: {
      const { contents: newContents, replaceLines } = change;
      return [
        ...contents.slice(0, lineNumber),
        ...newContents,
        ...contents.slice(lineNumber + replaceLines),
      ];
    }
    default:
      throw new Error(`Didn't handle change type ${FileChangeType[type]}`);
  }
};

const getPreviewFileContents = (
  steps: FileChange[],
  contents: string[]
): string[] => {
  if (!steps) {
    return contents;
  }

  return steps.reduce(applyPreviewStep, contents);
};

const isInRange = (length: number, index: number) =>
  index >= 0 && index < length;

const getRangeEnd = (
  contents: string[],
  index: number,
  idxUpdate: (idx: number) => number
): number => {
  let idx = index;
  let lastIdx = 0;
  const length = contents.length;
  let contentLinesRemaining = 4;

  while (isInRange(length, idx) && contentLinesRemaining > 0) {
    if (contents[idx].trim() !== '') {
      contentLinesRemaining--;
    }
    lastIdx = idx;
    idx = idxUpdate(idx);
  }

  return lastIdx;
};

const getContentLength = (change: FileChange) => {
  const { type } = change;
  switch (change.type) {
    case FileChangeType.Insertion:
      return change.contents.length;
    case FileChangeType.Modification:
      return change.contents.length;
    default:
      throw new Error(`Invalid change type ${FileChangeType[type]}`);
  }
};

const getPreviewRangeForStep = (
  contents: string[],
  change: FileChange
): [number, number] => {
  const contentLength = getContentLength(change);
  const { lineNumber } = change;
  const start = getRangeEnd(contents, lineNumber, (idx) => idx - 1);
  const end = getRangeEnd(
    contents,
    lineNumber + contentLength - 1,
    (idx) => idx + 1
  );
  return [start, end];
};

const overlaps = (
  [rangeStart, rangeEnd]: [number, number],
  [otherStart, otherEnd]: [number, number]
): boolean =>
  (rangeStart <= otherStart && otherStart <= rangeEnd) ||
  (otherStart <= rangeStart && rangeStart <= otherEnd);

const mergeRanges = (ranges: [number, number][]): [number, number][] => {
  for (let i = 0; i < ranges.length; i++) {
    const range = ranges[i];
    for (let j = i + 1; j < ranges.length; j++) {
      const other = ranges[j];
      if (overlaps(range, other)) {
        const start = Math.min(range[0], other[0]);
        const end = Math.max(range[1], other[1]);
        return mergeRanges([
          ...ranges.slice(0, i),
          [start, end],
          ...ranges.slice(i + 1, j),
          ...ranges.slice(j + 1),
        ]);
      }
    }
  }

  return ranges;
};

export type Range = [number, number];
export type RangeWithChangeType = [number, number, FileChangeType];
export type LineChangeTypes = { [line: number]: FileChangeType };
export interface Preview {
  previewContents: string[];
  lineChangeTypes: LineChangeTypes;
  displayChangeRanges: Range[];
}

const getPreviewsRangesForStepChanges = (
  contents: string[],
  steps: FileChange[]
): [Range[], LineChangeTypes] => {
  const stepRanges: RangeWithChangeType[] = steps.map((step) => {
    const [start, end] = getPreviewRangeForStep(contents, step);
    return [start, end, step.type];
  });
  const changeTypes = steps.reduce((map, step) => {
    const { lineNumber, type } = step;
    const range = getContentLength(step);
    for (let i = lineNumber; i < lineNumber + range; i++) {
      map[i] = type;
    }
    return map;
  }, {} as LineChangeTypes);
  return [
    mergeRanges(stepRanges.map(([start, end]) => [start, end])),
    changeTypes,
  ];
};

const getPreview = (steps: FileChange[][]): Preview => {
  const lastSteps = steps[steps.length - 1];
  const previewContents = getPreviewFileContents(
    lastSteps,
    getFileContents(steps.slice(0, steps.length - 1), [])
  );
  const [displayChangeRanges, lineChangeTypes] =
    getPreviewsRangesForStepChanges(previewContents, lastSteps);
  return {
    previewContents,
    lineChangeTypes,
    displayChangeRanges,
  };
};

export const walkthroughStepsService = (steps: FileChange[][]) => {
  return {
    getFileContents: () => getFileContents(steps, []),
    getPreviewForStepChanges: () => getPreview(steps),
  };
};

import { ServiceConfig } from '../infra-config/infra-config';
import {
  createInsert,
  createModify,
  DevelopmentWalkthrough,
  FileChange,
} from './development-walkthrough';

export enum Changes {
  AddFromSdk = 0,
  AddWorkdDir = 1,
  CopySrc = 2,
  Publish = 3,
  SwitchWorkDir = 4,
  AddEntryPoints = 5,
}

const getLines = (content: string): string[] => content.split(/\n/g);

const addFromSdk: FileChange[] = [
  createInsert(0, ['FROM mcr.microsoft.com/dotnet/sdk:5.0']),
];

const addWorkDir: FileChange[] = [
  createInsert(
    1,
    getLines(`
WORKDIR /app/src`)
  ),
];

const copySrc: FileChange[] = [createInsert(3, ['COPY . .'])];

const publish: FileChange[] = [
  createInsert(4, ['RUN dotnet publish -c Release -o /app/out']),
];

const switchWorkDir: FileChange[] = [createInsert(5, ['WORKDIR /app/out'])];

const addEntryPoint: FileChange[] = [
  createInsert(
    6,
    getLines(`
ENTRYPOINT ["dotnet", "DeploymentExample.dll"]`)
  ),
];

export const getDockerfileWalkthrough = (
  _?: ServiceConfig
): DevelopmentWalkthrough => {
  return {
    filename: 'Dockerfile',
    fileChangeSteps: [
      addFromSdk,
      addWorkDir,
      copySrc,
      publish,
      switchWorkDir,
      addEntryPoint,
    ],
  };
};

// FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

// WORKDIR /app/src

// # Copy everything else and build
// COPY . .
// RUN dotnet publish -c Release -o /app/out

// WORKDIR /app/out

// ENTRYPOINT ["dotnet", "DeploymentExample.dll"]

// # # Build runtime image
// # FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS bin

// # WORKDIR /app
// # COPY --from=build /app/out .

// # ENTRYPOINT ["dotnet", "DeploymentExample.dll"]

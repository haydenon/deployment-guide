import { ServiceConfig } from '../infra-config/infra-config';
import {
  createInsert,
  createModify,
  DevelopmentWalkthrough,
  FileChange,
} from './development-walkthrough';

export enum Changes {
  AddStages = 0,
  AddDeployJob = 1,
  AddExampleDeploy = 2,
  AddDeployJobImage = 3,
  AddDeployVariables = 4,
  AddBuildStage = 5,
  AddBuildJob = 6,
  AddDockerBuild = 7,
  AddDockerPush = 8,
  AddRegistryLogin = 9,
  ChangeToBuiltImage = 10,
}

const getLines = (content: string): string[] => content.split(/\n/g);

const addStages: FileChange[] = [
  createInsert(
    0,
    getLines(
      `stages:
  - deploy`
    )
  ),
];
const addDeployJob: FileChange[] = [
  createInsert(
    2,
    getLines(
      `
deploy-app:
  stage: deploy
  script:
    - echo "Hello world!"`
    )
  ),
];
const getAddExampleDeploy = (config: ServiceConfig): FileChange[] => [
  createModify(
    6,
    getLines(
      `    - |
      gcloud run deploy ${config.serviceName} \\
        --image=${config.repositoryBase}/example/app:automated \\
        --platform managed \\
        --region ${config.region} \\
        --project ${config.projectId}`
    )
  ),
];
const addDeployJobImage: FileChange[] = [
  createInsert(4, [
    '  image: registry.gitlab.com/haydenon/deployment-example-app/build/deploy',
  ]),
];

const addDeployVariables: FileChange[] = [
  createInsert(7, [
    '    - gcloud auth activate-service-account --key-file=$GOOGLE_APPLICATION_CREDENTIALS',
  ]),
  createModify(9, ['      gcloud run deploy $SERVICE_NAME \\']),
  createModify(10, ['        --image=$REPO_IMAGE:automated \\']),
  createModify(12, ['        --region $REGION \\']),
  createModify(13, ['        --project $PROJECT_ID']),
];

const addBuildStage: FileChange[] = [createInsert(1, ['  - build'])];

const addBuildJob: FileChange[] = [
  createInsert(
    3,
    getLines(`
build-app:
  stage: build
  image: registry.gitlab.com/haydenon/deployment-example-app/build/docker
  services:
    - docker:dind`)
  ),
];

const addDockerBuild: FileChange[] = [
  createInsert(
    9,
    getLines(
      `  script:
    - docker build -t $REPO_IMAGE:latest .`
    )
  ),
];

const addDockerPush: FileChange[] = [
  createInsert(11, ['    - docker push $REPO_IMAGE:latest']),
];

const addRegistryLogin: FileChange[] = [
  createInsert(
    10,
    getLines(
      `    - gcloud auth activate-service-account --key-file=$GOOGLE_APPLICATION_CREDENTIALS
    - gcloud auth print-access-token | docker login -u oauth2accesstoken --password-stdin $REGISTRY`
    )
  ),
];

const changeToBuiltImage: FileChange[] = [
  createModify(22, ['        --image=$REPO_IMAGE:latest \\']),
];

export const getGitlabCiWalkthrough = (
  config?: ServiceConfig
): DevelopmentWalkthrough => {
  if (!config) {
    return {
      filename: '.gitlab-ci.yml',
      fileChangeSteps: [],
    };
  }
  return {
    filename: '.gitlab-ci.yml',
    fileChangeSteps: [
      addStages,
      addDeployJob,
      getAddExampleDeploy(config),
      addDeployJobImage,
      addDeployVariables,
      addBuildStage,
      addBuildJob,
      addDockerBuild,
      addDockerPush,
      addRegistryLogin,
      changeToBuiltImage,
    ],
  };
};

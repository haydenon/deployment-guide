import { atom } from 'recoil';
import { createUninitialised, Item } from '../data/item';
import { getKeyGenerator, StateNamespace } from '../data/state-namespaces';
import { DeployKey, ServiceConfig } from './infra-config';

export enum StateKey {
  ServiceConfig = 'ServiceConfig',
  DeployKey = 'DeployKey',
}

const getKey = getKeyGenerator(StateNamespace.InfraConfig);

export const serviceConfigState = atom<Item<ServiceConfig>>({
  key: getKey(StateKey.ServiceConfig),
  default: createUninitialised(),
});

export const deployKeyState = atom<Item<DeployKey>>({
  key: getKey(StateKey.DeployKey),
  default: createUninitialised(),
});

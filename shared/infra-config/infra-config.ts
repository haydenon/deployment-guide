export interface ServiceConfig {
  projectId: number;
  region: string;
  serviceName: string;
  serviceUrl: string;
  repositoryBase: string;
  repositoryName: string;
}

export type DeployKey = any;

export interface InfraConfig {
  serviceConfig: ServiceConfig;
  deployKey: DeployKey;
}

import { useCallback, useEffect } from 'react';
import { useRecoilState } from 'recoil';
import { useFetch } from '../data/fetch.hook';
import { createCompleted, createLoading, ItemState } from '../data/item';

import { useUser } from '../user/user.hook';
import { DeployKey, ServiceConfig } from './infra-config';
import { deployKeyState, serviceConfigState } from './infra-config.state';

const basePath = '/api/infrastructure';

let triggered = false;

export const useInfraConfig = () => {
  const { fetch } = useFetch();
  const { isAuthorised } = useUser();
  const [serviceConfig, setService] = useRecoilState(serviceConfigState);
  const [deployUserKey, setKey] = useRecoilState(deployKeyState);

  const configState = serviceConfig.state;
  const keyState = deployUserKey.state;
  useEffect(() => {
    if (
      isAuthorised &&
      !triggered &&
      configState == ItemState.Uninitialised &&
      keyState == ItemState.Uninitialised
    ) {
      triggered = true;
      setService(createLoading());
      setKey(createLoading());

      Promise.all([
        fetch(`${basePath}/service/configuration`),
        fetch(`${basePath}/deploy-user/key`),
      ]).then(([serviceConfig, deployUserKey]: [ServiceConfig, DeployKey]) => {
        setService(createCompleted(serviceConfig));
        setKey(createCompleted(deployUserKey));
      });
    }
  }, [isAuthorised, triggered, fetch, configState, keyState]);

  return {
    serviceConfig,
    deployKey: deployUserKey,
  };
};

import { atom, useRecoilState } from 'recoil';
import { getKeyGenerator, StateNamespace } from './data/state-namespaces';

export enum StateKey {
  Title = 'Title',
}

const getKey = getKeyGenerator(StateNamespace.Title);

export const userState = atom<string>({
  key: getKey(StateKey.Title),
  default: 'Deploy Workshop',
});

export const useTitle = () => {
  const [titleValue, setTitle] = useRecoilState(userState);

  return {
    titleValue,
    setTitle,
  };
};

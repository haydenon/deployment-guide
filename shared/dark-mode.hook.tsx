import { useEffect } from 'react';

export const useDarkMode = () => {
  const isBrowser = process.browser;
  useEffect(() => {
    if (!isBrowser) {
      return;
    }

    const listener = (e: MediaQueryListEvent) => {
      if (e.matches) {
        document.body.classList.add('dark-theme');
        document.body.classList.remove('light-theme');
      } else {
        document.body.classList.add('light-theme');
        document.body.classList.remove('dark-theme');
      }
    };

    const matchMedia = window.matchMedia('(prefers-color-scheme: dark)');
    matchMedia.addEventListener('change', listener);

    if (matchMedia.matches) {
      document.body.classList.add('dark-theme');
      document.body.classList.remove('light-theme');
    } else {
      document.body.classList.add('light-theme');
      document.body.classList.remove('dark-theme');
    }

    return () => matchMedia.removeEventListener('change', listener);
  }, [isBrowser]);
};

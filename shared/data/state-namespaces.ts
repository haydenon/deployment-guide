export enum StateNamespace {
  User,
  InfraConfig,
  Title,
}

export const getKeyGenerator = (namespace: StateNamespace) => (key: string) =>
  `[${StateNamespace[namespace]}] ${key}`;

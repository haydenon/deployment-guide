import { useCallback } from 'react';
import { useRecoilState } from 'recoil';
import { useUser } from '../user/user.hook';
import { unauthorisedState } from '../user/user.state';

interface Config {
  headers?: {
    [key: string]: string;
  };
  mode?: 'no-cors' | 'cors' | 'same-origin';
  credentials?: 'same-origin' | 'include' | 'omit';
}

export class HttpError extends Error {
  constructor(message: string, public readonly status: number) {
    super(message);
  }
}

const handleErrors = (response: Response) => {
  if (!response.ok) {
    throw new HttpError(response.statusText, response.status);
  }

  return response;
};

export const useFetch = () => {
  const [_, setUnauthorised] = useRecoilState(unauthorisedState);
  const { user } = useUser();

  const doFetch = useCallback(
    (url: string, config?: Config) => {
      const token = user?.value?.tokenId;
      if (token) {
        config = {
          ...(config ?? {}),
          headers: {
            ...(config?.headers ?? {}),
            Authorization: `Bearer ${token}`,
          },
        };
      }

      return fetch(url, config)
        .then(handleErrors)
        .catch((err: HttpError) => {
          if (err.status === 403) {
            setUnauthorised(true);
          }

          throw err;
        })
        .then((resp) => resp.json());
    },
    [user]
  );

  return {
    fetch: doFetch,
  };
};

import localforage from 'localforage';
import { useCallback } from 'react';

import { DbKey } from './db-keys';

export const useDb = () => {
  const getItem = useCallback(<T>(key: DbKey) => {
    return localforage.getItem<T>(DbKey[key]);
  }, []);

  const setItem = useCallback(<T>(key: DbKey, value: T) => {
    return localforage.setItem<T>(DbKey[key], value);
  }, []);

  const removeItem = useCallback((key: DbKey) => {
    return localforage.removeItem(DbKey[key]);
  }, []);

  return {
    getItem,
    setItem,
    removeItem,
  };
};

import styled from 'styled-components';

const FullWidthWrapper = styled.div`
  max-width: 800px;
  margin: 0 auto;
  padding: var(--spacing-small);
`;

export default FullWidthWrapper;

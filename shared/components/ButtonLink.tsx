import styled from 'styled-components';
import Link from 'next/link';
import { buttonCommonStyles } from './Button';

interface Props {
  children: React.ReactNode | React.ReactNode[];
  link: string;
}

const LinkButton = styled.a`
  ${buttonCommonStyles}
  font-size: var(--typography-size-header-4);
  text-decoration: none;
  background-color: hsla(0deg 0% 0% / 0%);

  &:hover {
    text-decoration: none;
    background: var(--colors-button-transparent-hover);
  }

  &:active {
    text-decoration: none;
    background: var(--colors-button-transparent-active);
  }
`;

const ButtonLink = ({ children, link }: Props) => {
  return (
    <Link href={link} passHref>
      <LinkButton>{children}</LinkButton>
    </Link>
  );
};

export default ButtonLink;

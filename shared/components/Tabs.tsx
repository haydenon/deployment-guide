import {
  Tabs as ReachTabs,
  TabPanels,
  TabList,
  TabPanel,
  Tab,
} from '@reach/tabs';
import React from 'react';
import styled from 'styled-components';
import { buttonCommonStyles } from './Button';

export interface Tab {
  name: string;
  panel: () => JSX.Element;
}

interface TabProps {
  selected: boolean;
  tab: Tab;
}

const Button = styled(Tab)`
  ${buttonCommonStyles}
`;

const SelectedButton = styled(Button)`
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
  background-color: var(--colors-contentBackground);
  padding-bottom: var(--spacing-small);
`;

const UnselectedButton = styled(Button)`
  background-color: var(--colors-contentBackground-light);

  &:hover {
    background: var(--colors-contentBackground-light-focusable);
  }

  &:active {
    background: var(--colors-contentBackground-light-focused);
  }
`;

const ButtonWrapper = styled.span`
  padding-right: var(--spacing-tiny);
`;

const TabButton = ({ selected, tab: { name } }: TabProps) => {
  return (
    <ButtonWrapper>
      {selected ? (
        <SelectedButton>{name}</SelectedButton>
      ) : (
        <UnselectedButton>{name}</UnselectedButton>
      )}
    </ButtonWrapper>
  );
};

interface Props {
  tabs: Tab[];
}

const TabDisplay = styled(TabPanels)`
  background-color: var(--colors-contentBackground);
  padding: var(--spacing-small);
  border-radius: 0px var(--borders-radius) var(--borders-radius);
`;

const Tabs = ({ tabs }: Props) => {
  const [tabIndex, setTabIndex] = React.useState(0);
  return (
    <ReachTabs onChange={setTabIndex}>
      <TabList>
        {tabs.map((tab, idx) => (
          <TabButton
            selected={tabIndex === idx}
            key={idx}
            tab={tab}
          ></TabButton>
        ))}
      </TabList>
      <TabDisplay>
        {tabs.map((tab) => {
          const Panel = tab.panel;
          return (
            <TabPanel key={tab.name}>
              <Panel></Panel>
            </TabPanel>
          );
        })}
      </TabDisplay>
    </ReachTabs>
  );
};

export default Tabs;

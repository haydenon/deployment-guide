import Head from 'next/head';
import React from 'react';
import styled from 'styled-components';
import { breakpoints } from '../../styles/theme';
import { useTitle } from '../title.hook';

import AccountNav from '../user/AccountNav/AccountNav';
import FullWidthWrapper from './FullWidthWrapper';

const Nav = styled.nav`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`;

const Heading = styled.h1`
  padding-bottom: 0;
`;

const HeadingIcon = styled.span`
  display: none;
  @media (min-width: ${breakpoints.xsmall}) {
    display: inline;
  }
`;

const HeadingWrapper = styled(FullWidthWrapper)`
  @media (min-width: ${breakpoints.large}) {
    width: 100%;
    max-width: calc(${breakpoints.large} - 50px);
  }
`;

const Header = () => {
  const { titleValue } = useTitle();
  return (
    <>
      <Head>
        <title key="title">{titleValue}</title>
      </Head>
      <HeadingWrapper>
        <Nav>
          <Heading>
            Deploys <HeadingIcon>🚀</HeadingIcon>
          </Heading>
          <AccountNav></AccountNav>
        </Nav>
      </HeadingWrapper>
    </>
  );
};

export default Header;

import {
  Menu,
  MenuButton,
  MenuPopover,
  MenuItems,
  MenuItem as UnstyledMenutItem,
  useMenuButtonContext,
} from '@reach/menu-button';
import styled from 'styled-components';

interface Props {
  renderButton: () => React.ReactNode | React.ReactNode[];
  position?: (
    targetRect?: DOMRect | null,
    popoverRect?: DOMRect | null
  ) => React.CSSProperties;
  children: React.ReactNode | React.ReactNode[];
}

const StyledMenuButton = styled(MenuButton)`
  background-color: hsla(0deg 0% 0% / 0%);
  cursor: pointer;
  border: none;
  display: flex;
  align-items: center;
`;

interface ButtonProps {
  children: React.ReactNode | React.ReactNode[];
  showCaret?: boolean;
}

const CaretWrapper = styled.span`
  vertical-align: middle;
  font-size: 20px;
  padding-left: var(--spacing-tiny);

  transition: 0.1s;

  &.expanded {
    transform: rotateX(180deg) translate(0, -5px);
  }
`;

export const DropdownButton = ({ children, showCaret }: ButtonProps) => {
  const { isExpanded } = useMenuButtonContext();
  return (
    <StyledMenuButton>
      {children}
      {showCaret || showCaret === undefined ? (
        <CaretWrapper aria-hidden className={isExpanded ? 'expanded' : ''}>
          ▾
        </CaretWrapper>
      ) : null}
    </StyledMenuButton>
  );
};

interface ItemProps {
  onSelect: () => void;
  children: React.ReactNode | React.ReactNode[];
}

const MenuItem = styled(UnstyledMenutItem)`
  display: block;
  color: inherit;
  font: inherit;

  padding: var(--spacing-tiny) var(--spacing-small);
`;

const MenuItemButton = styled(UnstyledMenutItem)`
  display: block;
  color: inherit;
  font: inherit;
  padding: var(--spacing-tiny) var(--spacing-small);
  cursor: pointer;
  outline: none;
  background: var(--colors-background);
  border: none;

  &:hover {
    background: var(--colors-background-focusable);
    outline: none;
  }

  &:active {
    background: var(--colors-background-focused);
    outline: none;
  }
`;

export const DropdownItem = ({ onSelect, children }: ItemProps) => {
  return <MenuItemButton onSelect={onSelect}>{children}</MenuItemButton>;
};

interface DisplayItemProps {
  children: React.ReactNode | React.ReactNode[];
}

export const DropdownDisplayItem = ({ children }: DisplayItemProps) => {
  return (
    <MenuItem onSelect={() => {}} disabled={true}>
      {children}
    </MenuItem>
  );
};

const DropdownCard = styled.div`
  padding: var(--spacing-tiny) 0;
  background-color: var(--colors-background);
  border: var(--borders-width) solid var(--colors-text);
  border-radius: var(--borders-radius);
`;

const Dropdown = ({ renderButton, position, children }: Props) => {
  return (
    <Menu>
      {renderButton()}
      <MenuPopover position={position as any}>
        <DropdownCard>
          <MenuItems>{children}</MenuItems>
        </DropdownCard>
      </MenuPopover>
    </Menu>
  );
};

export default Dropdown;

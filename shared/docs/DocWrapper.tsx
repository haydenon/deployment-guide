import { useRouter } from 'next/router';
import NextLink from 'next/link';
import styled, { css } from 'styled-components';

import { breakpoints } from '../../styles/theme';
import FullWidthWrapper from '../components/FullWidthWrapper';
import { useUser } from '../user/user.hook';

const ContentWrapper = styled(FullWidthWrapper)`
  display: flex;
  flex-direction: column;
  @media (min-width: ${breakpoints.large}) {
    justify-content: space-between;
    width: calc(${breakpoints.large} - 50px);
    max-width: calc(${breakpoints.large} - 50px);
    flex-direction: row;
  }
`;

const Sidebar = styled.div`
  padding: var(--spacing-base) 0;
  flex: 1 1 auto;
  @media (min-width: ${breakpoints.large}) {
    padding: var(--spacing-small) var(--spacing-base) 0 0;
    flex: 1 1 300px;
  }
`;

const Menu = styled.div`
  @media (min-width: ${breakpoints.large}) {
    top: 0;
    position: sticky;
    width: 300px;
  }
`;

const MenuHeading = styled.h2`
  font-size: var(--typography-size-header-3);
  padding-top: var(--spacing-tiny);
  padding-bottom: var(--spacing-base);
`;

const Contents = styled.div`
  flex: 1 1 auto;
  width: 100%;
  padding-top: 0;
  @media (min-width: ${breakpoints.large}) {
    width: 800px;
    flex: 1 1 800px;
  }
`;

const LinkList = styled.ol`
  margin: 0;
  list-style-type: none;
  padding: 0 0 var(--spacing-base) 0;
`;

const LinkItem = styled.li`
  padding-bottom: var(--spacing-small);
`;

const baseLink = css`
  font-size: 1.2rem;
  color: var(--colors-text);
`;

const Link = styled.a`
  ${baseLink}
  &:hover {
    cursor: pointer;
  }
`;

const ActiveLink = styled.span`
  ${baseLink}
  font-weight: 500;
  text-decoration: underline;
`;

interface LinkProps {
  page: string;
  children: React.ReactNode | React.ReactNode[];
}
const PageLink = ({ page, children }: LinkProps) => {
  const router = useRouter();
  const currentPage = router.pathname;
  if (currentPage === page) {
    return <ActiveLink>{children}</ActiveLink>;
  }

  return (
    <NextLink href={page} passHref>
      <Link>{children}</Link>
    </NextLink>
  );
};

interface Props {
  children: React.ReactChild | React.ReactChild[];
}

const DocWrapper = ({ children }: Props) => {
  const { isSignedIn, isUnauthorised } = useUser();
  const router = useRouter();

  if (isSignedIn === false || isUnauthorised) {
    router.push({ pathname: '/' });
  }

  return (
    <ContentWrapper>
      <Sidebar>
        <Menu>
          <MenuHeading>Pages</MenuHeading>
          <LinkList>
            <LinkItem>
              <PageLink page="/manual-deploy">1. Manual Deploys</PageLink>
            </LinkItem>
            <LinkItem>
              <PageLink page="/automated-deploy">2. Automated Deploys</PageLink>
            </LinkItem>
            <LinkItem>
              <PageLink page="/building-image">3. Building Images</PageLink>
            </LinkItem>
          </LinkList>
        </Menu>
      </Sidebar>
      <Contents>{children}</Contents>
    </ContentWrapper>
  );
};

export default DocWrapper;

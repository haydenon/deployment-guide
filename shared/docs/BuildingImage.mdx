import {
  Tester,
  DeployKey,
  GcpLink,
  Img,
  ExampleDeploy,
  ServiceName,
  Repository,
  Registry,
  ProjectId,
  Region,
} from '../md-components';
import { ExampleRepository } from '../md-components/Repositories';
import { CloudRunLink } from '../md-components/Links';
import FileChange, { Ci, Docker } from '../md-components/FileChange';

## Building a New Service Image

We've automated the deployment of our service, but we're only deploying a pre-built image.
Let's go ahead and build the container image from our latest project source code and deploy that.

### Creating a Dockerfile

We're going to use Docker to build our container, so let's go ahead and creat an empty file called `Dockerfile` at
the root our our project repository. A `Dockerfile` is a series of commands and options that modify the state of the container.
Each change that we make is then baked into the new container image.

First of all, we need to choose a base image to build off of.
Since we're building a .NET 5 application, we should use the correct SDK image for that.

<FileChange step={Docker.AddFromSdk}></FileChange>

Then, let's set our working directory, much like changing into that directory in a command line.
The directory will be created in the process, and futher commands and options will be run from it.

<FileChange step={Docker.AddWorkdDir}></FileChange>

Because our container only starts off with what it was constructed with, it doesn't have any of the contents of our project inside it.
We'll need to copy everything we want over into it.

This command will copy the current folder on the computer running Docker along with all its contents,
which is the first `.`. We then want that copy to end up in the current working directory inside the container, which is the second `.`.

<FileChange step={Docker.CopySrc}></FileChange>

Next, we'll run the `dotnet publish` command to build our project.

<FileChange step={Docker.Publish}></FileChange>

Then we'll update our working directory to be the directory that our build was outputted into.

<FileChange step={Docker.SwitchWorkDir}></FileChange>

Lastly, we need to tell our container what to do when it's started up.
We do this by giving it an entrypoint command.

<FileChange step={Docker.AddEntryPoints}></FileChange>

This `Dockerfile` is quite specific to a .NET container, but other languages tend to follow a similar structure

1. Build from a relevant base image
2. Copy the source code
3. Compile the source
4. Setup the application

At the moment we're also using the SDK image to run the application as well. While not ideal, we'll roll with it for this demo.

### Deploy Job Updates

With that our `Dockerfile` should be ready to go, but we'll need to hook up our pipeline to build and push it to Google Cloud.
Let's go ahead and add a `build` stage before `deploy` in our `.gitlab-ci.yml` configuration.

<FileChange step={Ci.AddBuildStage}></FileChange>

And following that, let's add a job to do the build in this stage. This job will be a little different to the previous one.
We'll give it a stage of `build`, but because it will require Docker and we're running our jobs on GitLab using Docker images,
we'll need to use Docker-in-Docker mode.

Because of this, we'll use a different `image`: one with Docker installed, and we'll also tell
GitLab we're wanting to use the `docker:dind` service for this job (`dind` standing for Docker-in-Docker.)

<FileChange step={Ci.AddBuildJob}></FileChange>

We'll also need to add a script to build a Docker image using our `Dockerfile`. We'll use the `docker build` command to do this,
using the `-t` parameter to give the resulting image a tag name which is our `$REPO_IMAGE` variable, with the tag of `latest` - the default
Docker tag to indicate it's the most recent image.

The `.` at the end of the command means we're wanting to run the build with the context
of our current directory. By default `docker build` will also use the file called `Dockerfile` in the directory it is run.

<FileChange step={Ci.AddDockerBuild}></FileChange>

Once we've built our image, we'll also want to push it up to Google Cloud. Because `$REPO_IMAGE` contains the Google Cloud registry
in it's path, it'll know to send it there.

<FileChange step={Ci.AddDockerPush}></FileChange>

Since that repo is private, we'll need to login though. Let's setup the credentials again, and then run a command to login to the
Google Cloud Docker registry.

<FileChange step={Ci.AddRegistryLogin}></FileChange>

Lastly, since we've changed the tag we're using to `latest`, we'll need to update the deploy job's tag from the old `automated` tag as well.

<FileChange step={Ci.ChangeToBuiltImage}></FileChange>

However, a couple of things have changed. We're switching over from the common readonly `example` registry to a service specific repository for our Docker images.

Go ahead and open up the `Settings > CI/CD` page on your GitLab project to make some more changes.
We'll need to update our variable for `REPO_IMAGE` to our service's specific repository: <Repository image="app"></Repository>

We also added a reference to a variable called `REGISTRY`, so go and add another variable with that name (no `$`) with the value of <Registry/>
(we could've derived this from the start of `REPO_IMAGE`, but we're just adding another variable for simplicity.)

Phew, that's a lot of changes. After updating the variables, go ahead and stage all the changes with `git add .`, then commit and push.

If you go back to your GitLab project, click on `CI/CD` in the sidebar, you should see a new pipeline with multiple jobs

<Img
  image="building-image/build-deploy-pipeline"
  title="Pipeline with Build & Deploy Job"
  maxWidth={600}
></Img>

Clicking on the pipeline id, you should see both jobs you defined under it.

<Img
  image="building-image/pipeline-jobs"
  title="Build & Deploy Job"
  maxWidth={600}
></Img>

If you then open up the `build-app` job and watch it, you should eventually see the docker image being built.

<Img
  image="building-image/docker-build"
  title="Docker Build Log"
  maxWidth={800}
></Img>

Once the build job has finished, you can go back and watch the deploy job to see progress.

<Img
  image="building-image/pipeline-success"
  title="Pipeline Success"
  maxWidth={400}
></Img>

Once they've both successfully run, your service should now be using your newly built image.

Let's try it out...

<Tester></Tester>

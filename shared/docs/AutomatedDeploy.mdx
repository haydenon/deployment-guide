import {
  Tester,
  DeployKey,
  GcpLink,
  Img,
  ExampleDeploy,
  ServiceName,
  Repository,
  ProjectId,
  Region,
} from '../md-components';
import { ExampleRepository } from '../md-components/Repositories';
import { CloudRunLink } from '../md-components/Links';
import FileChange, { Ci } from '../md-components/FileChange';

## Automating Deploys

We're going to go ahead and automate that deploy using a GitLab pipeline. At this stage we won't be building our own image, we'll get there later.

First of all, open up your local forked copy of the `https://gitlab.com/haydenon/deployment-example-app` repo in your favourite editor.
Create an empty file at the root of it with the name `.gitlab-ci.yml`. This is the file that will define all the jobs you want to run in your pipeline.

### Define Your First Job

The first thing we need to define is the different stages for our pipeline. For now, we're just wanting to deploy a pre-built image so we'll just
create a single stage stage called `deploy`.

⚠ Identation is important for YAML files. Try make sure you stick to the indentation provided.

<FileChange step={Ci.AddStages}></FileChange>

Now we'll create a job called `deploy-app`, which runs for the `deploy`. We'll give it a basic script so we can verify it's working.

<FileChange step={Ci.AddDeployJob}></FileChange>

Once you've added these to your project, go ahead and add, commit and push it to your forked repository.

If you're unfamiliar with using git, you can use the following commands to stage, commit and then push your changes:

`git add .`

`git commit -m 'Added a demo deploy job'`

`git push`

After you've pushed your changes, go to your project on GitLab
and click on the `CI/CD` menu option on the sidebar.

<Img
  image="automated-deploy/project-sidebar-ci"
  title="GitLab CI/CD Menu Option"
  maxWidth={600}
></Img>

You'll see that a pipeline was created

<Img
  image="automated-deploy/deploy-demo-pipeline"
  title="Demo Deploy Pipeline"
  maxWidth={600}
></Img>

And if you click on the pipeline ID link, you'll see that it contained a single job called `deploy-app` defined in the `.gitlab-ci.yml`.
You'll also notice that this pipeline is associated with your commit.

<Img
  image="automated-deploy/deploy-demo-jobs"
  title="Demo Deploy Jobs"
  maxWidth={300}
></Img>

Clicking on the `deploy-app` job will take you through to the job logs for that job.

<Img
  image="automated-deploy/deploy-demo-job-log"
  title="Demo Deploy Job Log"
  maxWidth={600}
></Img>

Now that we've acquainted ourselves with pipelines, we'll move on to automating the deploy.

### Deploying to Cloud Run

Let's go ahead and have a go at changing the job to actually deploy to Cloud Run.

We'll use the `gcloud` command-line SDK to run our deploy from our job. We're wanting to deploy the `app` image with the tag `automated`.
Looking at [the reference](https://cloud.google.com/sdk/gcloud/reference/run/deploy), it looks like we'll want to use
something like <ExampleDeploy></ExampleDeploy>, so let's give that a go by changing our demo script to what we want.

<FileChange step={Ci.AddExampleDeploy}></FileChange>

Phew, that's a big change! Normally you'd have a line per command e.g.

```
script:
  - echo "Hello..."
  - echo "World!"
```

But since our one command got unbearably long, we've changed it to be a multi-line command.

By default we're given a base image that doesn't include include the `gcloud` tool or other tools we want. We could install
these as part of the job, but to speed things up let's use an image for the job that comes with the `gcloud` tool installed.

<FileChange step={Ci.AddDeployJobImage}></FileChange>

Go ahead and commit and push those changes, then pop over to the pipelines page on GitLab for your project.

Let's see how this goes... 🤞

<Img
  image="automated-deploy/deploy-job-no-creds"
  title="Deploy Job with No Credentials"
  maxWidth={600}
></Img>

Oh no, our runner doesn't have access to deploy the service - we haven't given it any credentials...

### Configuring our Pipelines

We'll need to configure our GitLab project with our credentials so that we can give our pipelines access to go ahead and deploy on
our behalf. Navigate to the `Settings` > `CI/CD` page on the sidebar in GitLab.

<Img
  image="automated-deploy/project-sidebar-settings-ci"
  title="CI/CD Settings"
  maxWidth={400}
></Img>

Go ahead and open the variables section and select the option to add a variable.

<Img
  image="automated-deploy/project-ci-settings-variables"
  title="CI/CD Settings"
  maxWidth={750}
></Img>

Let's go ahead and add your credentials. Give them a name called `GOOGLE_APPLICATION_CREDENTIALS`, change the variable type to `File`.
The rest of the defaults are fine - but make sure you leave the protected option selected, since your credentials are sensitive.

Go ahead and copy the contents of your credential file out of here and into the value field. Once you've done that go ahead and save it.

<DeployKey></DeployKey>

While we're here, we may as well change some of the other values into variables. Go ahead and add
a variable with each of the following details, leaving the type as variable. These ones aren't sensitive,
but feel free to leave them protected.

| Name           | Value                                          |
| -------------- | ---------------------------------------------- |
| `SERVICE_NAME` | <ServiceName/>                                 |
| `REPO_IMAGE`   | <Repository repository="example" image="app"/> |
| `REGION`       | <Region/>                                      |
| `PROJECT_ID`   | <ProjectId/>                                   |

It should end up looking something like:

<Img
  image="automated-deploy/project-ci-settings"
  title="CI/CD Settings"
  maxWidth={750}
></Img>

Let's go ahead and change our script to activate our credentials, and use the other variables we added

<FileChange step={Ci.AddDeployVariables}></FileChange>

Once you've finished, commit and push your changes to trigger another pipeline attempt.

### Trying Again

This time, everything else going right, it should pass.

<Img
  image="automated-deploy/deploy-success-job-log"
  title="CI/CD Settings"
  maxWidth={600}
></Img>

Yay! 🥳

Once you've confirmed that it has finised deploying, go ahead and test it again.

<Tester />

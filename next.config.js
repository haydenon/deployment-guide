const withMDX = require('@next/mdx')({
  extension: /\.mdx$/,
});

module.exports = withMDX({
  async rewrites() {
    return process.env.API_URI
      ? [
          {
            source: '/api/:path*',
            destination: `${process.env.API_URI}/:path*`,
          },
        ]
      : [];
  },
  distDir: 'out',
  target: 'serverless',
  pageExtensions: ['js', 'jsx', 'ts', 'tsx', 'md', 'mdx'],
});

import { createGlobalStyle } from 'styled-components';
import { codeStyles } from './theme';

export const GlobalStyle = createGlobalStyle`
  html {
    font-size: 1.125rem;
    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
      Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
      sans-serif;
  }

  html,
  body,
  body > div {
    padding: 0;
    margin: 0;
    height: 100%;
  }

  body {
    background-color: var(--colors-background);
    color: var(--colors-text);
  }

  button {
    color: var(--colors-text);
  }

  .app-container {
    min-height: 100%;
  }

  h1, h2, h3, h4, h5 {
    margin: 0;
    padding: var(--spacing-base) 0
  }

  p {
    margin: 0;
    line-height: var(--typography-lineHeight);
  }

  p:not(:last-child) {
    padding-bottom: var(--spacing-small);
  }

  textarea, input {
    font-size: 1rem;
    line-height: var(--typography-lineHeight);
  }

  h1 {
    font-size: var(--typography-size-header-1);
  }

  h2 {
    font-size: var(--typography-size-header-2);
  }

  h3 {
    font-size: var(--typography-size-header-3);
  }

  h4 {
    font-size: var(--typography-size-header-4);
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
  }

  code, pre {
    ${codeStyles}
  }

  a {
    color: var(--colors-text-link);
    text-decoration: none;
    &:hover {
      text-decoration: underline;
    }
  }

  td, th {
    padding-bottom: var(--spacing-tiny);
    &:not(:last-child) {
      padding-right: var(--spacing-small);
    }
  }

  table {
    padding-bottom: var(--spacing-base);
  }
`;

import { createGlobalStyle, css } from 'styled-components';

const darkTheme = css`
  /* Background  */
  --colors-background: hsl(200deg 15% 10%);
  --colors-background-focusable: hsl(200deg 15% 18%);
  --colors-background-focused: hsl(200deg 15% 14%);

  /* Content background  */
  --colors-contentBackground: hsl(200deg 5% 20%);
  --colors-contentBackground-focusable: hsl(200deg 5% 28%);
  --colors-contentBackground-focused: hsl(200deg 5% 24%);

  --colors-contentBackground-light: hsl(200deg 5% 30%);
  --colors-contentBackground-light-focusable: hsl(200deg 5% 36%);
  --colors-contentBackground-light-focused: hsl(200deg 5% 33%);
  --colors-contentBackground-light-disabled: hsl(200deg 5% 60%);

  --colors-contentBackground-dark: hsl(200deg 5% 15%);

  --colors-contentBackground-positive: hsl(115deg 20% 35%);
  --colors-contentBackground-neutral: hsl(45deg 45% 40%);
  --colors-contentBackground-negative: hsl(200deg 5% 20%);

  --colors-text: hsla(0deg 0% 100% / 95%);
  --colors-text-small: hsla(0deg 0% 100% / 85%);
  --colors-text-link: hsl(233deg 83% 70%);
  --colors-text-disabled: hsla(0deg 0% 100% / 70%);

  /* Buttons */
  --colors-button-transparent-hover: hsl(0deg 0% 100% / 10%);
  --colors-button-transparent-active: hsl(0deg 0% 100% / 5%);
`;

export const ThemeStyle = createGlobalStyle`
:root {
  /* Spacing */
  --spacing-tiny: 8px;
  --spacing-small: 16px;
  --spacing-base: 24px;
  --spacing-large: 32px;
  --spacing-extraLarge: 40px;

  /* Borders */
  --borders-radius: 4px;
  --borders-width: 1px;

  /* Typography */
  --typography-size-header-1: 2.8rem;
  --typography-size-header-2: 2.4rem;
  --typography-size-header-3: 1.8rem;
  --typography-size-header-4: 1.4rem;
  --typography-size-paragraph: 1rem;
  --typography-size-small: 0.75rem;
  --typography-lineHeight: 1.4;

  --focus-color: hsl(240deg 70% 80%);

    ${darkTheme}
}

`;

export const codeStyles = css`
  user-select: all;
  background-color: var(--colors-contentBackground);
  border-radius: var(--borders-radius);
  padding: 0px 6px;
`;

export const breakpoints = {
  large: '1200px',
  small: '675px',
  xsmall: '375px',
};

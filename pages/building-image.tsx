import React, { useEffect } from 'react';
import styled from 'styled-components';
import ButtonLink from '../shared/components/ButtonLink';
import BuildingImage from '../shared/docs/BuildingImage.mdx';
import DocWrapper from '../shared/docs/DocWrapper';
import { useTitle } from '../shared/title.hook';

const DocContents = styled.div`
  padding-bottom: var(--spacing-extraLarge);
`;

const NextLink = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-bottom: var(--spacing-large);
`;

export default function ManualDeployPage() {
  const { setTitle } = useTitle();
  useEffect(() => {
    setTitle('Building Images');
  }, [setTitle]);

  return (
    <DocWrapper>
      <DocContents>
        <BuildingImage></BuildingImage>
      </DocContents>
      {/* <NextLink>
        <ButtonLink link="/building-image">On to Building Images →</ButtonLink>
      </NextLink> */}
    </DocWrapper>
  );
}

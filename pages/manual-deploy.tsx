import React, { useEffect } from 'react';
import styled from 'styled-components';
import ButtonLink from '../shared/components/ButtonLink';
import DocWrapper from '../shared/docs/DocWrapper';
import ManualDeploy from '../shared/docs/ManualDeploy.mdx';
import { useTitle } from '../shared/title.hook';

const DocContents = styled.div`
  padding-bottom: var(--spacing-extraLarge);
`;

const NextLink = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-bottom: var(--spacing-large);
`;

export default function ManualDeployPage() {
  const { setTitle } = useTitle();
  useEffect(() => {
    setTitle('Manual Deploys');
  }, [setTitle]);

  return (
    <DocWrapper>
      <DocContents>
        <ManualDeploy></ManualDeploy>
      </DocContents>
      <NextLink>
        <ButtonLink link="/automated-deploy">
          On to Automated Deploys →
        </ButtonLink>
      </NextLink>
    </DocWrapper>
  );
}

import type { AppProps } from 'next/app';
import Head from 'next/head';
import { RecoilRoot } from 'recoil';

import { GlobalStyle } from '../styles/global';
import { ThemeStyle } from '../styles/theme';

import Header from '../shared/components/Header';
import { useDarkMode } from '../shared/dark-mode.hook';

export default function App({ Component, pageProps }: AppProps) {
  useDarkMode();
  return (
    <RecoilRoot>
      <div className="app-container">
        <Head>
          <link rel="icon" href="/favicon.ico" />
        </Head>
        <GlobalStyle />
        <ThemeStyle />
        <Header></Header>
        <Component {...pageProps} />
      </div>
    </RecoilRoot>
  );
}

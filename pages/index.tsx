import Head from 'next/head';
import { useRouter } from 'next/router';
import styled from 'styled-components';

import FullWidthWrapper from '../shared/components/FullWidthWrapper';

import { useUser } from '../shared/user/user.hook';

const TextDisplay = styled.p`
  font-size: var(--typography-size-header-4);
`;

const Display = () => {
  const { isUnauthorised, isSignedIn } = useUser();
  const router = useRouter();

  if (isUnauthorised) {
    return (
      <TextDisplay>
        Uh oh, you haven't been added to this applicaton yet, sorry! 😞
      </TextDisplay>
    );
  }

  if (isSignedIn === true) {
    router.push({ pathname: '/manual-deploy' });
    return null;
  }

  return <TextDisplay>Welcome! Go ahead and sign in to start.</TextDisplay>;
};

const Wrapper = styled(FullWidthWrapper)`
  padding-top: 100px;
  text-align: center;
`;

export default function Home() {
  return (
    <>
      <Head>
        <title key="title">Deployment guide</title>
      </Head>
      <Wrapper>
        <Display></Display>
      </Wrapper>
    </>
  );
}
